# Target Interface
class MediaPlayer:
    def play(self):
        pass


# Adaptee Classes
class MP3Player:
    def playMP3(self):
        print("Playing MP3 Audio")


class MP4Player:
    def playMP4(self):
        print("Playing MP4 Video")


# Adaptee Classes
class MP3Adapter(MediaPlayer):
    def __init__(self, mp3Player):
        self._mp3Player = mp3Player

    def play(self):
        self._mp3Player.playMP3()


class MP4Adapter(MediaPlayer):
    def __init__(self, mp4Player):
        self._mp4Player = mp4Player

    def play(self):
        self._mp4Player.playMP4()


# Client Code
mp4Player = MP4Player()
mp4Adapter = MP4Adapter(mp4Player)
mp4Adapter.play()

mp3Player = MP3Player()
mp3Adapter = MP3Adapter(mp3Player)
mp3Adapter.play()
